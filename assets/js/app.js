require('./bootstrap');

Vue.component('comment', require('./components/Comment.vue'));
Vue.component('comments', require('./components/Comments.vue'));

const eventHub = new Vue();

const app = new Vue({
    el: '#app'
});
