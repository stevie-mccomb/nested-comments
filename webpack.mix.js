const mix = require('laravel-mix');

mix
    .options({ processCssUrls: false, publicPath: 'public' })
    .copy('node_modules/font-awesome/fonts', 'public/fonts')
    .js('assets/js/app.js', 'public/js')
    .sass('assets/sass/app.scss', 'public/css');
